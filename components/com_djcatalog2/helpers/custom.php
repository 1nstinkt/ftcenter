<?php
defined('_JEXEC') or die('Restricted access');

class DJCatalog2CustomHelper {
	const STATUS_FIELD_ID = 20;

	public static function getCustomStatuses() {
		$db = JFactory::getDbo();

		// Create a new query object.
		$query = $db->getQuery(true);
		// Order it by the ordering field.
		$query->select($db->quoteName(array('value')));
		$query->from($db->quoteName('#__djc2_items_extra_fields_options'));
		$query->where($db->quoteName('field_id') . ' = '. $db->quote(self::STATUS_FIELD_ID));
		$query->order('ordering ASC');

		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		return $db->loadColumn();
	}
}