<?php
    
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

?>

<form class="uk-form" action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post">

	<?php if ($params->get('pretext')) : ?>
	<div class="uk-form-row">
		<?php echo $params->get('pretext'); ?>
	<?php endif; ?>

		<input class="uk-width-1-1" type="text" name="username" size="18" style="width: 150px; height: 20px; padding: 0; padding-left: 7px; line-height: 20px; font-size: 12px;" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME') ?>">

		<input class="uk-width-1-1" type="password" name="password" size="18" style="width: 150px; height: 20px; padding: 0; padding-left: 7px; line-height: 20px; font-size: 12px;" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>">

		<button class="uk-button uk-button-primary" value="<?php echo JText::_('JLOGIN') ?>" name="Submit" style="min-height: 20px; height: 20px; font-size: 12px" type="submit"><?php echo JText::_('JLOGIN') ?></button>

<?php $usersConfig = JComponentHelper::getParams('com_users'); ?>
<?php if ($usersConfig->get('allowUserRegistration')) : ?>
<a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>" style="font-size: 12px"><?php echo JText::_('MOD_LOGIN_REGISTER'); ?></a>
<?php endif; ?>
</div>




	
	<?php if($params->get('posttext')) : ?>
	<div class="uk-form-row">
		<?php echo $params->get('posttext'); ?>
	</div>
	<?php endif; ?>
	
	<input type="hidden" name="option" value="com_users">
	<input type="hidden" name="task" value="user.login">
	<input type="hidden" name="return" value="<?php echo $return; ?>">
	<?php echo JHtml::_('form.token'); ?>
</form>
