# Changelog
	
	1.0.3
	+ Added alignment classes (WP)

	1.0.2
	# Fixed Logo-Small position

	1.0.1
	# Display Social Buttons correctly in posts
	# Fixed article meta display option
	
	1.0.0
	+ Initial Release



	* -> Security Fix
	# -> Bug Fix
	$ -> Language fix or change
	+ -> Addition
	^ -> Change
	- -> Removed
	! -> Note