<?php
/**
 * @version $Id: default_files.php 87 2012-08-02 12:13:07Z michal $
 * @package DJ-Catalog2
 * @copyright Copyright (C) 2012 DJ-Extensions.com LTD, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Michal Olczyk - michal.olczyk@design-joomla.eu
 *
 * DJ-Catalog2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-Catalog2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-Catalog2. If not, see <http://www.gnu.org/licenses/>.
 *
 */

defined ('_JEXEC') or die('Restricted access'); ?>

<div class="djc_files afiles">
<ul>
<?php foreach($this->item->files as $file) {?>
	<li>
		<div>
			<a target="_blank" href="<?php echo ('index.php?option=com_djcatalog2&format=raw&task=download&fid='.$file->id);?>">
				<?php echo $file->caption; ?>
			</a>
		</div>

	</li>
<?php } ?>
</ul>
</div>