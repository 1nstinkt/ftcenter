<?php
/**
 * @version $Id: default.php 146 2013-10-07 09:02:25Z michal $
 * @package DJ-Catalog2
 * @copyright Copyright (C) 2012 DJ-Extensions.com LTD, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Michal Olczyk - michal.olczyk@design-joomla.eu
 *
 * DJ-Catalog2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-Catalog2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-Catalog2. If not, see <http://www.gnu.org/licenses/>.
 *
 */

define('AUCTIONS_CATEGORY', 	1);
define('REALTYOBJS_CATEGORY', 	3);
define('ORDERS_CATEGORY', 		4);
define('ORDERPLANS_CATEGORY', 	5);

defined ('_JEXEC') or die('Restricted access');
$user		= JFactory::getUser();
$price_auth = ($this->params->get('price_restrict', '0') == '1' && $user->guest) ? false : true;
$edit_auth = ($user->authorise('core.edit', 'com_djcatalog2') || ($user->authorise('core.edit.own', 'com_djcatalog2') && $user->id == $this->item->created_by)) ? true : false;
$category_id = JFactory::getApplication()->input->get('cid'); // id of product category
$another_description = "";
$related_object_attributes = "";
$document = JFactory::getDocument();
$url = JUri::base() . 'templates/ftcentre/css/popup.css';
$document->addStyleSheet($url);
$url = JUri::base() . 'templates/ftcentre/js/jquery.popup.min.js';
$document->addScript($url);
$object_link = "";
?>
	<script>
		function wordwrap( str, width, brk, cut ) {
			brk = brk || '\n';
			width = width || 75;
			cut = cut || false;

			if (!str) { return str; }

			var regex = '.{1,' +width+ '}(\\s|$)' + (cut ? '|.{' +width+ '}|.+$' : '|\\S+?(\\s|$)');

			return str.match( RegExp(regex, 'g') ).join( brk );
		}

		jQuery(document).ready(function($){
			$('.tm-breadcrumb').prependTo('.tm-middle.uk-grid .tm-main.uk-width-medium-1-1 .tm-content');
			var map_link = "<div class='map_link'><span>&nbsp;</span>посмотреть карту</div>";

			// for realty objects
			$('.componentheadingrealtyobj-page').html(wordwrap($('h2.djc_title').text(), 50, '<br/>\n')+map_link);
			$('h2.djc_title').text('');

			var map,
				Placemark;

			var popup = new $.Popup({
				afterOpen: function() {
					var x_coord = $("#map_coords").attr("data-x");
					var y_coord = $("#map_coords").attr("data-y");
					Placemark = new ymaps.Placemark([y_coord, x_coord]);
					map = new ymaps.Map("map", {
						center: [
							y_coord,
							x_coord
						],
						zoom: 12
					});
					map.geoObjects.add(Placemark);
				}
			});
			$('div.map_link').click(function(){
				if($("#map_coords").length) {
					setTimeout(function(){
						popup.open('<div id="map" style="width: 750px; height: 600px;"></div>', 'html');
					}, 1000);
				}
			});

			// for auctions
			var object_link = $("#realty_object_link").val();
			var auction_link = "<a class='object_link' href='"+object_link+"'><span>&nbsp;</span>подробнее об объекте</a>";
			$('.componentheadingtab-page').html(wordwrap($('span.object_address').text(), 50, '<br/>\n')+auction_link);
			$('span.object_address').text('');

		});
	</script>
	<style type="text/css">
		.djc_images {
			margin: 15px 0 20px 0 !important;
			float: left !important;
		}

		.djc_attributes table tbody td {
			padding: 8px 16px;
			color: #152f4a;
		}
		.djc_attributes table {
			font-size: 13px;
			text-align: center;
		}
		.djc_attributes table thead th {
			padding: 16px 16px;
			color: #152f4a;
		}
		.djc_attributes table thead th{
			font-weight: normal !important;
			font-size: 13px;
		}
		.djc_attributes table tbody tr td:first-child a {
			color: #152f4a;
			font-weight: bold;
		}
		.djc_attributes table tbody tr:nth-child(odd) {
			background: #cbdeed !important;
		}
		.djc_attributes table tbody tr:nth-child(odd) td:first-child { border-top-left-radius: 10px; }
		.djc_attributes table tbody tr:nth-child(odd) td:last-child { border-top-right-radius: 10px; }
		.djc_attributes table tbody tr:nth-child(odd) td:first-child { border-bottom-left-radius: 10px; }
		.djc_attributes table tbody tr:nth-child(odd) td:last-child { border-bottom-right-radius: 10px; }

		.tm-content .componentheadingrealtyobj-page {
			line-height: 42px;
			padding-top: 75px;
			height: 120px;
			position: relative;
		}
		.table.table-condensed {
			box-shadow: none;
		}
		.djc_attributes table tr.djc_attribute:nth-child(2n+1) td.djc_label {
			background: none;
			box-shadow: none;
		}
		.djc_attributes table tr.djc_attribute td.djc_label {
			background: none;
			box-shadow: none;
		}
		.djc_attributes table tr.djc_attribute:nth-child(2n+1) td.djc_value {
			background: none;
		}
		.djc_description {
			color: #003156 !important;
		}
		.object_info_container {
			border-bottom: 1px dotted;
			float: left;
			font-weight: bold;
		}

		.tm-content .componentheadingtab-page {
			padding-top: 75px;
			height: 120px;
			position: relative;
			font-size: 28px !important;
			line-height: 42px;
			height: 120px !important;
		}
		.related_object_attributes {
			margin: 0 !important;
		}

	</style>
<?php
if($category_id == REALTYOBJS_CATEGORY) {
	if ($this->params->get( 'show_page_heading', 1) /*&& ($this->params->get( 'page_heading') != @$this->item->name)*/) : ?>
		<h1 class="componentheading<?php echo $this->params->get( 'pageclass_sfx' ) ?>">
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h1>
	<?php endif; ?>

	<div id="djcatalog" class="djc_clearfix djc_item<?php echo $this->params->get( 'pageclass_sfx' ).' djc_theme_'.$this->params->get('theme','default'); if ($this->item->featured == 1) echo ' featured_item'; ?>">
		<?php if($this->item->event->beforeDJCatalog2DisplayContent) { ?>
			<div class="djc_pre_content">
				<?php echo $this->item->event->beforeDJCatalog2DisplayContent; ?>
			</div>
		<?php } ?>
		<?php if ($this->navigation && (!empty($this->navigation['prev']) || !empty($this->navigation['next'])) && ($this->params->get('show_navigation', '0') == 'top' || $this->params->get('show_navigation', '0') == 'all')) { ?>
			<div class="djc_product_top_nav djc_clearfix">
				<?php if (!empty($this->navigation['prev'])) { ?>
					<a class="djc_prev_btn" href="<?php echo JRoute::_(DJCatalogHelperRoute::getItemRoute($this->navigation['prev']->slug, $this->navigation['prev']->catslug)); ?>"><span class="button btn"><?php echo JText::_('COM_DJCATALOG2_PREVIOUS'); ?></span></a>
				<?php } ?>
				<?php if (!empty($this->navigation['next'])) { ?>
					<a class="djc_next_btn" href="<?php echo JRoute::_(DJCatalogHelperRoute::getItemRoute($this->navigation['next']->slug, $this->navigation['next']->catslug)); ?>"><span class="button btn"><?php echo JText::_('COM_DJCATALOG2_NEXT'); ?></span></a>
				<?php } ?>
			</div>
		<?php } ?>
		<?php if ( in_array('item', $this->params->get('social_code_views',array())) && $this->params->get('social_code_position','top') == 'top' && $this->params->get('social_code', '') != '') { ?>
			<div class="djc_clearfix djc_social_t">
				<?php echo $this->params->get('social_code'); ?>
			</div>
		<?php } ?>
		<div class="uk-grid" style="margin: 35px 0 35px 0;">
			<div class="uk-width-medium-6-10">
				<h2 class="djc_title">
					<?php if ($this->item->featured == 1) {
						echo '<img class="djc_featured_image" alt="'.JText::_('COM_DJCATALOG2_FEATURED_ITEM').'" title="'.JText::_('COM_DJCATALOG2_FEATURED_ITEM').'" src="'.DJCatalog2ThemeHelper::getThemeImage('featured.png').'" />';
					}?>
					<?php echo $this->item->name; ?>
				</h2>
				<?php if ((int)$this->params->get('fed_edit_button', 0) == 1 && $edit_auth) { ?>
					<a class="btn btn-primary btn-mini button djc_edit_button" href="<?php echo JRoute::_('index.php?option=com_djcatalog2&task=itemform.edit&id='.$this->item->id); ?>"><?php echo JText::_('COM_DJCATALOG2_EDIT')?></a>
				<?php } ?>
				<?php if($this->item->event->afterDJCatalog2DisplayTitle) { ?>
					<div class="djc_post_title">
						<?php echo $this->item->event->afterDJCatalog2DisplayTitle; ?>
					</div>
				<?php } ?>

				<?php if ( in_array('item', $this->params->get('social_code_views',array())) && $this->params->get('social_code_position','top') == 'aft_title' && $this->params->get('social_code', '') != '') { ?>
					<div class="djc_clearfix djc_social_at">
						<?php echo $this->params->get('social_code'); ?>
					</div>
				<?php } ?>

				<div class="djc_description">
					<?php if ($this->params->get('show_category_name_item') && $this->item->publish_category == '1') { ?>
						<div class="djc_category_info">
							<small>
								<?php
								if ($this->params->get('show_category_name_item') == 2) {
									echo JText::_('COM_DJCATALOG2_CATEGORY').': '?><span><?php echo $this->item->category; ?></span>
								<?php }
								else {
									echo JText::_('COM_DJCATALOG2_CATEGORY').': ';?><a href="<?php echo DJCatalogHelperRoute::getCategoryRoute($this->item->catslug);?>"><span><?php echo $this->item->category; ?></span></a>
								<?php } ?>
							</small>
						</div>
					<?php } ?>
					<?php if ($this->params->get('show_producer_name_item') > 0 && $this->item->publish_producer == '1' && $this->item->producer) { ?>
						<div class="djc_producer_info">
							<small>
								<?php
								if ($this->params->get('show_producer_name_item') == 2) {
									echo JText::_('COM_DJCATALOG2_PRODUCER').': '; ?><span><?php echo $this->item->producer;?></span>
								<?php }
								else if(($this->params->get('show_producer_name_item') == 3)) {
									echo JText::_('COM_DJCATALOG2_PRODUCER').': ';?><a class="modal" rel="{handler: 'iframe', size: {x: 800, y: 450}}" href="<?php echo JRoute::_(DJCatalogHelperRoute::getProducerRoute($this->item->prodslug).'&tmpl=component'); ?>"><span><?php echo $this->item->producer; ?></span></a>
								<?php }
								else {
									echo JText::_('COM_DJCATALOG2_PRODUCER').': ';?><a href="<?php echo JRoute::_(DJCatalogHelperRoute::getProducerRoute($this->item->prodslug)); ?>"><span><?php echo $this->item->producer; ?></span></a>
								<?php } ?>
								<?php if ($this->params->get('show_producers_items_item', 1)) { ?>
									<a class="djc_producer_items_link btn btn-mini button" href="<?php echo JRoute::_(DJCatalogHelperRoute::getCategoryRoute(0).'&cm=0&pid='.$this->item->producer_id); ?>"><span><?php echo JText::_('COM_DJCATALOG2_SHOW_PRODUCERS_ITEMS'); ?></span></a>
								<?php } ?>
							</small>
						</div>
					<?php } ?>
					<?php
					// simply hide price
					if (0 && $price_auth && ($this->params->get('show_price_item') == 2 || ( $this->params->get('show_price_item') == 1 && $this->item->price > 0.0))) {
						?>
						<div class="djc_price">
							<small>
								<?php
								if ($this->item->price != $this->item->final_price ) { ?>
									<?php if ($this->params->get('show_old_price_item', '1') == '1') {?>
										<?php echo JText::_('COM_DJCATALOG2_PRICE').': ';?><span class="djc_price_old"><?php echo DJCatalog2HtmlHelper::formatPrice($this->item->price, $this->params); ?></span>&nbsp;<span class="djc_price_new"><?php echo DJCatalog2HtmlHelper::formatPrice($this->item->final_price, $this->params); ?></span>
									<?php } else { ?>
										<?php echo JText::_('COM_DJCATALOG2_PRICE').': ';?><span><?php echo DJCatalog2HtmlHelper::formatPrice($this->item->final_price, $this->params); ?></span>
									<?php } ?>
								<?php } else { ?>
									<?php echo JText::_('COM_DJCATALOG2_PRICE').': ';?><span><?php echo DJCatalog2HtmlHelper::formatPrice($this->item->price, $this->params); ?></span>
								<?php } ?>
							</small>
						</div>
					<?php
					} ?>

					<?php
					if (count($this->attributes) > 0) {
						$attributes_body = '';
						foreach ($this->attributes as $attribute) {
							$this->attribute_cursor = $attribute;
							if($attribute->alias == 'koordinaty') continue;
							$attributes_body .= $this->loadTemplate('attributes');
						}
						?>
						<?php if ($attributes_body != '') { ?>
							<div class="djc_attributes">
								<table class="table table-condensed">
									<?php echo $attributes_body; ?>
								</table>
							</div>
						<?php }
						$coords = explode(',', $this->item->{'_ef_koordinaty'});
						if($coords[1]) {
							$document = JFactory::getDocument();
							$document->addScript("http://api-maps.yandex.ru/2.1/?lang=ru_RU");
							echo '
								<div id="map_coords" style="display:none;" data-x="'.$coords[1].'" data-y="'.$coords[0].'"></div>
							';
						}
						?>
					<?php } ?>
					<div class="djc_fulltext">
						<?php echo JHTML::_('content.prepare', $this->item->description); ?>
					</div>

					<?php if (isset($this->item->tabs)) { ?>
						<div class="djc_clear"></div>
						<div class="djc_tabs">
							<?php echo JHTML::_('content.prepare', $this->item->tabs); ?>
						</div>
					<?php } ?>
					<?php if ($this->item->files = DJCatalog2FileHelper::getFiles('item',$this->item->id)) {
						echo $this->loadTemplate('files');
					} ?>
					<?php if ($this->params->get('show_contact_form', '1')) { ?>
						<div class="djc_clear"></div>
						<div class="djc_contact_form_wrapper" id="contactform">
							<?php echo $this->loadTemplate('contact'); ?>
						</div>
					<?php } ?>
					<?php if($this->item->event->afterDJCatalog2DisplayContent) { ?>
						<div class="djc_post_content">
							<?php echo $this->item->event->afterDJCatalog2DisplayContent; ?>
						</div>
					<?php } ?>

					<?php if ($this->navigation && (!empty($this->navigation['prev']) || !empty($this->navigation['next'])) && ($this->params->get('show_navigation', '0') == 'bottom' || $this->params->get('show_navigation', '0') == 'all')) { ?>
						<div class="djc_product_bottom_nav djc_clearfix">
							<?php if (!empty($this->navigation['prev'])) { ?>
								<a class="djc_prev_btn" href="<?php echo JRoute::_(DJCatalogHelperRoute::getItemRoute($this->navigation['prev']->slug, $this->navigation['prev']->catslug)); ?>"><span class="button btn"><?php echo JText::_('COM_DJCATALOG2_PREVIOUS'); ?></span></a>
							<?php } ?>
							<?php if (!empty($this->navigation['next'])) { ?>
								<a class="djc_next_btn" href="<?php echo JRoute::_(DJCatalogHelperRoute::getItemRoute($this->navigation['next']->slug, $this->navigation['next']->catslug)); ?>"><span class="button btn"><?php echo JText::_('COM_DJCATALOG2_NEXT'); ?></span></a>
							<?php } ?>
						</div>
					<?php } ?>

					<?php if ( in_array('item', $this->params->get('social_code_views',array())) && $this->params->get('social_code_position','top') == 'aft_desc' && $this->params->get('social_code', '') != '') { ?>
						<div class="djc_clearfix djc_social_ad">
							<?php echo $this->params->get('social_code'); ?>
						</div>
					<?php } ?>

					<?php if((int)$this->params->get('comments', 0) > 0){
						echo $this->loadTemplate('comments');
					} ?>

					<?php if ($this->relateditems && $this->params->get('related_items_count',2) > 0) {
						echo $this->loadTemplate('relateditems');
					} ?>
				</div>
			</div>
			<div class="uk-width-medium-4-10">
				<?php
				$this->item->images = DJCatalog2ImageHelper::getImages('item',$this->item->id);
				if ($this->item->images && (int)$this->params->get('show_image_item', 1) > 0) {
					echo $this->loadTemplate('images');
				}
				?>
				<?php if ($this->params->get('show_contact_form', '1')) { ?>
					<p class="djc_contact_form_toggler">
						<button style="background-color: #d33325; padding: 15px;" id="djc_contact_form_button" class="btn btn-primary btn-mini button"><?php echo JText::_('COM_DJCATALOG2_CONTACT_FORM_OPEN')?></button>
					</p>
				<?php } ?>
			</div>
		</div>


		<?php if ( in_array('item', $this->params->get('social_code_views',array())) && $this->params->get('social_code_position','top') == 'bottom' && $this->params->get('social_code', '') != '') { ?>
			<div class="djc_clearfix djc_social_b">
				<?php echo $this->params->get('social_code'); ?>
			</div>
		<?php } ?>
		<?php
		if ($this->params->get('show_footer')) echo DJCATFOOTER;
		?>
	</div>
<?php
} else {
	if ($this->params->get( 'show_page_heading', 1) /*&& ($this->params->get( 'page_heading') != @$this->item->name)*/) : ?>
		<h1 class="componentheading<?php echo $this->params->get( 'pageclass_sfx' ) ?>">
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h1>
	<?php endif; ?>
	<div id="djcatalog" class="djc_clearfix djc_item<?php echo $this->params->get( 'pageclass_sfx' ).' djc_theme_'.$this->params->get('theme','default'); if ($this->item->featured == 1) echo ' featured_item'; ?>">
	<?php if($this->item->event->beforeDJCatalog2DisplayContent) { ?>
		<div class="djc_pre_content">
			<?php echo $this->item->event->beforeDJCatalog2DisplayContent; ?>
		</div>
	<?php } ?>
	<?php if ($this->navigation && (!empty($this->navigation['prev']) || !empty($this->navigation['next'])) && ($this->params->get('show_navigation', '0') == 'top' || $this->params->get('show_navigation', '0') == 'all')) { ?>
		<div class="djc_product_top_nav djc_clearfix">
			<?php if (!empty($this->navigation['prev'])) { ?>
				<a class="djc_prev_btn" href="<?php echo JRoute::_(DJCatalogHelperRoute::getItemRoute($this->navigation['prev']->slug, $this->navigation['prev']->catslug)); ?>"><span class="button btn"><?php echo JText::_('COM_DJCATALOG2_PREVIOUS'); ?></span></a>
			<?php } ?>
			<?php if (!empty($this->navigation['next'])) { ?>
				<a class="djc_next_btn" href="<?php echo JRoute::_(DJCatalogHelperRoute::getItemRoute($this->navigation['next']->slug, $this->navigation['next']->catslug)); ?>"><span class="button btn"><?php echo JText::_('COM_DJCATALOG2_NEXT'); ?></span></a>
			<?php } ?>
		</div>
	<?php } ?>
	<?php if ( in_array('item', $this->params->get('social_code_views',array())) && $this->params->get('social_code_position','top') == 'top' && $this->params->get('social_code', '') != '') { ?>
		<div class="djc_clearfix djc_social_t">
			<?php echo $this->params->get('social_code'); ?>
		</div>
	<?php } ?>
	<?php
	$this->item->images = DJCatalog2ImageHelper::getImages('item',$this->item->id);
	if ($this->item->images && (int)$this->params->get('show_image_item', 1) > 0) {
		echo $this->loadTemplate('images');
	} ?>
	<h2 class="djc_title">
		<?php if ($this->item->featured == 1) {
			echo '<img class="djc_featured_image" alt="'.JText::_('COM_DJCATALOG2_FEATURED_ITEM').'" title="'.JText::_('COM_DJCATALOG2_FEATURED_ITEM').'" src="'.DJCatalog2ThemeHelper::getThemeImage('featured.png').'" />';
		}?>
		<?php echo $this->item->name; ?>
	</h2>
	<?php if ((int)$this->params->get('fed_edit_button', 0) == 1 && $edit_auth) { ?>
		<a class="btn btn-primary btn-mini button djc_edit_button" style="margin: 0 0 15px 0;" href="<?php echo JRoute::_('index.php?option=com_djcatalog2&task=itemform.edit&id='.$this->item->id); ?>"><?php echo JText::_('COM_DJCATALOG2_EDIT')?></a>
	<?php } ?>
	<?php if($this->item->event->afterDJCatalog2DisplayTitle) { ?>
		<div class="djc_post_title">
			<?php echo $this->item->event->afterDJCatalog2DisplayTitle; ?>
		</div>
	<?php } ?>

	<?php if ( in_array('item', $this->params->get('social_code_views',array())) && $this->params->get('social_code_position','top') == 'aft_title' && $this->params->get('social_code', '') != '') { ?>
		<div class="djc_clearfix djc_social_at">
			<?php echo $this->params->get('social_code'); ?>
		</div>
	<?php } ?>

	<?php if ($this->params->get('show_contact_form', '1')) { ?>
		<p class="djc_contact_form_toggler" style="display: none;">
			<button id="djc_contact_form_button" class="btn btn-primary btn-mini button"><?php echo JText::_('COM_DJCATALOG2_CONTACT_FORM_OPEN')?></button>
		</p>
	<?php } ?>

	<div class="djc_description">
	<?php if ($this->params->get('show_category_name_item') && $this->item->publish_category == '1') { ?>
		<div class="djc_category_info">
			<small>
				<?php
				if ($this->params->get('show_category_name_item') == 2) {
					echo JText::_('COM_DJCATALOG2_CATEGORY').': '?><span><?php echo $this->item->category; ?></span>
				<?php }
				else {
					echo JText::_('COM_DJCATALOG2_CATEGORY').': ';?><a href="<?php echo DJCatalogHelperRoute::getCategoryRoute($this->item->catslug);?>"><span><?php echo $this->item->category; ?></span></a>
				<?php } ?>
			</small>
		</div>
	<?php } ?>
	<?php if ($this->params->get('show_producer_name_item') > 0 && $this->item->publish_producer == '1' && $this->item->producer) { ?>
		<div class="djc_producer_info">
			<small>
				<?php
				if ($this->params->get('show_producer_name_item') == 2) {
					echo JText::_('COM_DJCATALOG2_PRODUCER').': '; ?><span><?php echo $this->item->producer;?></span>
				<?php }
				else if(($this->params->get('show_producer_name_item') == 3)) {
					echo JText::_('COM_DJCATALOG2_PRODUCER').': ';?><a class="modal" rel="{handler: 'iframe', size: {x: 800, y: 450}}" href="<?php echo JRoute::_(DJCatalogHelperRoute::getProducerRoute($this->item->prodslug).'&tmpl=component'); ?>"><span><?php echo $this->item->producer; ?></span></a>
				<?php }
				else {
					echo JText::_('COM_DJCATALOG2_PRODUCER').': ';?><a href="<?php echo JRoute::_(DJCatalogHelperRoute::getProducerRoute($this->item->prodslug)); ?>"><span><?php echo $this->item->producer; ?></span></a>
				<?php } ?>
				<?php if ($this->params->get('show_producers_items_item', 1)) { ?>
					<a class="djc_producer_items_link btn btn-mini button" href="<?php echo JRoute::_(DJCatalogHelperRoute::getCategoryRoute(0).'&cm=0&pid='.$this->item->producer_id); ?>"><span><?php echo JText::_('COM_DJCATALOG2_SHOW_PRODUCERS_ITEMS'); ?></span></a>
				<?php } ?>
			</small>
		</div>
	<?php } ?>
	<?php
	if (0 && $price_auth && ($this->params->get('show_price_item') == 2 || ( $this->params->get('show_price_item') == 1 && $this->item->price > 0.0))) {
		?>
		<div class="djc_price">
			<small>
				<?php
				if ($this->item->price != $this->item->final_price ) { ?>
					<?php if ($this->params->get('show_old_price_item', '1') == '1') {?>
						<?php echo JText::_('COM_DJCATALOG2_PRICE').': ';?><span class="djc_price_old"><?php echo DJCatalog2HtmlHelper::formatPrice($this->item->price, $this->params); ?></span>&nbsp;<span class="djc_price_new"><?php echo DJCatalog2HtmlHelper::formatPrice($this->item->final_price, $this->params); ?></span>
					<?php } else { ?>
						<?php echo JText::_('COM_DJCATALOG2_PRICE').': ';?><span><?php echo DJCatalog2HtmlHelper::formatPrice($this->item->final_price, $this->params); ?></span>
					<?php } ?>
				<?php } else { ?>
					<?php echo JText::_('COM_DJCATALOG2_PRICE').': ';?><span><?php echo DJCatalog2HtmlHelper::formatPrice($this->item->price, $this->params); ?></span>
				<?php } ?>
			</small>
		</div>
	<?php
	} ?>
	<div class="djc_fulltext">
		<div class="uk-grid">
			<div class="uk-width-medium-6-10">
				<?php
				if($category_id == AUCTIONS_CATEGORY) {
					$related_object_attributes = '
									<div class="djc_attributes related_object_attributes">
										<table class="table table-condensed">
								';
					$item = $this->item;
					$realty_item_id = (!empty($item->{'_ef_id_pomeshcheniya'})) ? $item->{'_ef_id_pomeshcheniya'} : 0;
					if($realty_item_id) {
						$realty_item = get_related_object_item($realty_item_id);
						$object_link = JRoute::_(DJCatalogHelperRoute::getItemRoute($realty_item->slug, $realty_item->catslug ));

						foreach ($this->attributes as $attribute) {
							$attribute_alias = '_ef_'.$attribute->alias;
							if(!empty($realty_item->$attribute_alias)) {
								$attr_value = "";
								$attribute_value = "";
								$attribute_name = $attribute->name;

								if (is_array($realty_item->$attribute_alias)){
									if($attribute_alias == '_ef_imeetsya') {
										$icons_to_attribute_value = array(
											'электроэнергия' => 'electric',
											'клининговые_услуги' => 'cleaning',
											'телефония' => 'telephony',
											'интернет' => 'internet',
											'мебель' => 'furniture',
											'охрана' => 'security',
											'кондиционировение' => 'conditioner',
											'вентиляция_воздуха' => 'ventilation'
										);
										$attribute_value = "";
										foreach($realty_item->$attribute_alias as $index => $value) {
											$attribute_value .= "<span class='realty_obj_has class_".$icons_to_attribute_value[$value]."' title='{$value}'></span>";
										}
										$realty_item->$attribute_alias = JHTML::_('content.prepare', $attribute_value);

									} else {
										$realty_item->$attribute_alias = implode(', ', $realty_item->$attribute_alias);
									}
								}

								if ($attribute->type == 'textarea' || $attribute->type == 'text'){
									$attr_value = nl2br(htmlspecialchars($realty_item->$attribute_alias));
									// convert URLs
									$attr_value = preg_replace('#([\w]+://)([^\s()<>]+)#iS', '<a target="_blank" href="$1$2">$2</a>', $attr_value);
									// convert emails
									$attr_value = preg_replace('#([\w.-]+(\+[\w.-]+)*@[\w.-]+)#i', '<a target="_blank" href="mailto:$1">$1</a>', $attr_value);
									$attribute_value = $attr_value;
								} else if ($attribute->type == 'html') {
									$attribute_value = JHTML::_('content.prepare', $realty_item->$attribute_alias);
								} else if ($attribute->type == 'calendar') {
									if(!empty($item->$attribute_alias))
										$attribute_value = JHtml::_('date', $realty_item->$attribute_alias, JText::_('DATE_FORMAT_LC4'));
								} else if($attribute_alias != '_ef_imeetsya') {
									$attribute_value = htmlspecialchars($realty_item->$attribute_alias);
								} else {
									$attribute_value = $realty_item->$attribute_alias;
								}

								switch($attribute->alias) {
									case 'koordinaty':
										/*$coords = explode(',', $attribute_value);
										$another_description .= '<div class="object_map"><img style="width: 100%;" src="http://static-maps.yandex.ru/1.x/?lang=ru-RU&amp;ll='.
												$coords[1].','.$coords[0].'&amp;z=15&amp;l=map&amp;size=650,450&amp;pt='.$coords[1].','.$coords[0].',pm2lbm" alt="" /></div>';*/
										break;
									case 'adres_ob_ekta':
										$another_description .= '<p style="display: none;"><span class="fs-28px object_address">'.$attribute_value.'</span></p>';
										break;
									default:
										if ($attribute->imagelabel != '') {
											$related_object_attributes .= '<tr class="djc_attribute djc'.$attribute_alias.'">'.
													'<td class="djc_label">'.
													'<img class="djc_attribute-imglabel" alt="'.htmlspecialchars($attribute_name).'" src="'.JURI::base().$attribute->imagelabel.'" />';
										} else {
											$related_object_attributes .= '<tr class="djc_attribute djc'.$attribute_alias.'">'.
													'<td class="djc_label">'.
													'<span class="djc_attribute-label">'.htmlspecialchars($attribute_name).'</span>';
										}
										$related_object_attributes .= "</td>";
										$related_object_attributes .=  '<td  class="djc_value">'.
												$attribute_value.'</td>'.
												'</tr>';
										break;
								}
								//echo $this->loadTemplate('header');
							}
						}
						$related_object_attributes .= "</table></div>";
						/*$another_description .= '<div class="uk-grid">';
						$another_description .= '<div class="uk-width-medium-6-10">&nbsp;'.$realty_item->description.'</div>';

						$this->item->images = DJCatalog2ImageHelper::getImages('item', $realty_item_id);
						if ($this->item->images && (int)$this->params->get('show_image_item', 1) > 0) {
							$another_description .= "<div class='uk-width-medium-4-10'>".$this->loadTemplate('images')."</div>";
						}
						$another_description .= '</div>';*/
					}
					echo JHTML::_('content.prepare', $another_description.$related_object_attributes);
					echo "<input type='hidden' id='realty_object_link' value='{$object_link}'>";
				}
				if (count($this->attributes) > 0) {
					$attributes_body = '';
					foreach ($this->attributes as $attribute) {
						$this->attribute_cursor = $attribute;
						$attributes_body .= $this->loadTemplate('attributes');
					}
					?>
					<?php if ($attributes_body != '') { ?>
						<div class="djc_attributes">
							<table class="table table-condensed">
								<?php echo $attributes_body; ?>
							</table>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<div class="uk-width-medium-4-10" style="margin-top: 15px;">
				<?php if ($this->item->files = DJCatalog2FileHelper::getFiles('item',$this->item->id)) {
					echo $this->loadTemplate('afiles');
				} ?>
			</div>
		</div>
	</div>


	<?php if($category_id == AUCTIONS_CATEGORY) { ?>
	<div style="display: table; margin: 0 0 15px 0;">
		<div class="object_info_container">Информация об аукционе</div>
	</div>
	<?php } ?>

	<?php echo JHTML::_('content.prepare', $this->item->description); ?>

	<?php if (isset($this->item->tabs)) { ?>
		<div class="djc_clear"></div>
		<div class="djc_tabs">
			<?php echo JHTML::_('content.prepare', $this->item->tabs); ?>
		</div>
	<?php } ?>
	<?php if ($this->params->get('show_contact_form', '1')) { ?>
		<div class="djc_clear"></div>
		<div class="djc_contact_form_wrapper" id="contactform">
			<?php echo $this->loadTemplate('contact'); ?>
		</div>
	<?php } ?>
	<?php if($this->item->event->afterDJCatalog2DisplayContent) { ?>
		<div class="djc_post_content">
			<?php echo $this->item->event->afterDJCatalog2DisplayContent; ?>
		</div>
	<?php } ?>

	<?php if ($this->navigation && (!empty($this->navigation['prev']) || !empty($this->navigation['next'])) && ($this->params->get('show_navigation', '0') == 'bottom' || $this->params->get('show_navigation', '0') == 'all')) { ?>
		<div class="djc_product_bottom_nav djc_clearfix">
			<?php if (!empty($this->navigation['prev'])) { ?>
				<a class="djc_prev_btn" href="<?php echo JRoute::_(DJCatalogHelperRoute::getItemRoute($this->navigation['prev']->slug, $this->navigation['prev']->catslug)); ?>"><span class="button btn"><?php echo JText::_('COM_DJCATALOG2_PREVIOUS'); ?></span></a>
			<?php } ?>
			<?php if (!empty($this->navigation['next'])) { ?>
				<a class="djc_next_btn" href="<?php echo JRoute::_(DJCatalogHelperRoute::getItemRoute($this->navigation['next']->slug, $this->navigation['next']->catslug)); ?>"><span class="button btn"><?php echo JText::_('COM_DJCATALOG2_NEXT'); ?></span></a>
			<?php } ?>
		</div>
	<?php } ?>

	<?php if ( in_array('item', $this->params->get('social_code_views',array())) && $this->params->get('social_code_position','top') == 'aft_desc' && $this->params->get('social_code', '') != '') { ?>
		<div class="djc_clearfix djc_social_ad">
			<?php echo $this->params->get('social_code'); ?>
		</div>
	<?php } ?>

	<?php if((int)$this->params->get('comments', 0) > 0){
		echo $this->loadTemplate('comments');
	} ?>

	<?php if ($this->relateditems && $this->params->get('related_items_count',2) > 0) {
		echo $this->loadTemplate('relateditems');
	} ?>
	</div>

	<?php if ( in_array('item', $this->params->get('social_code_views',array())) && $this->params->get('social_code_position','top') == 'bottom' && $this->params->get('social_code', '') != '') { ?>
		<div class="djc_clearfix djc_social_b">
			<?php echo $this->params->get('social_code'); ?>
		</div>
	<?php } ?>
	<?php
	if ($this->params->get('show_footer')) echo DJCATFOOTER;
	?>
	</div>

<?php
}
function get_related_object_item($pid) {
	require_once(JPATH_COMPONENT.DS.'models'.DS.'item.php');

	$related_item_model = new DJCatalog2ModelItem();
	return $related_item_model->getItemByFK($pid);
}