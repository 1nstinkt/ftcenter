<?php
/**
 * @version $Id: default_items.php 114 2013-02-01 10:04:49Z michal $
 * @package DJ-Catalog2
 * @copyright Copyright (C) 2012 DJ-Extensions.com LTD, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Michal Olczyk - michal.olczyk@design-joomla.eu
 *
 * DJ-Catalog2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-Catalog2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-Catalog2. If not, see <http://www.gnu.org/licenses/>.
 *
 */
define('AUCTIONS_CATEGORY',		1);
define('REALTYOBJS_CATEGORY', 	3);
define('ORDERS_CATEGORY', 		4);
define('ORDERPLANS_CATEGORY', 	5);

defined('_JEXEC') or die('Restricted access');
$user = JFactory::getUser();
$price_auth = ($this->params->get('price_restrict', '0') == '1' && $user->guest) ? false : true;
$current_creator = 0;
?>
	<style type="text/css">
		.zakypki tbody td {
			padding: 8px 16px;
			color: #152f4a;
		}
		.table_tab {
			overflow: auto;
		}
		.zakypki {
			font-size: 13px;
			text-align: center;
		}
		.zakypki thead th {
			padding: 16px 16px;
			color: #152f4a;
		}
		.zakypki thead th{
			font-weight: normal !important;
			font-size: 13px;
		}
		.zakypki tbody tr td:first-child a {
			color: #152f4a;
			font-weight: bold;
		}
		.zakypki tbody tr:nth-child(odd) {
			background: #cbdeed !important;
		}
		.zakypki tbody tr:nth-child(odd) td:first-child { border-top-left-radius: 10px; }
		.zakypki tbody tr:nth-child(odd) td:last-child { border-top-right-radius: 10px; }
		.zakypki tbody tr:nth-child(odd) td:first-child { border-bottom-left-radius: 10px; }
		.zakypki tbody tr:nth-child(odd) td:last-child { border-bottom-right-radius: 10px; }

		.zakypki tbody tr td:first-child {
			word-wrap: break-word;
			word-break: break-all;
		}

		.ui-helper-reset {
			font-size: 13px !important;

		}
		.ui-tabs-anchor {
			font-weight: bolder !important;
		}
		.ui-tabs-panel {
			min-height: 500px !important;
		}
		.submit_btn {
			border-radius: 5px;
			-moz-border-radius: 5px;
			-webkit-border-radius: 5px;
		}
		.tm-content .componentheadingrealtyobj-page {
			line-height: 195px !important;
		}

	</style>
	<script>
		function change_listing_count(el) {
			if(jQuery(el).val() == 'all') {
				var new_count = 'all';
			} else {
				var new_count = parseInt(jQuery(el).val());
			}

			var url = window.location.toString();
			if(/limit=[^\&]*/.exec(url)) {
				window.location = url.replace(/limit=[^\&]*/, 'limit='+new_count);
			} else {
				if(url.indexOf("?") > 0) {
					window.location = url+'&limit='+new_count;
				} else {
					window.location = url+'?limit='+new_count;
				}

			}

		}
        jQuery(document).ready(function($){
            $('.tm-breadcrumb').prependTo('.tm-middle.uk-grid .tm-main.uk-width-medium-1-1 .tm-content');
            var items = '<?php echo json_encode($this->items, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE); ?>';
			if($('.djc_pagination').length && $('.moderation_container').length ) {
				$('.moderation_container').insertAfter($('.djc_pagination'));
			}
        });
	</script>
<?php
$document = JFactory::getDocument();
$url = JUri::base() . 'templates/ftcentre/css/custom.css';
$document->addStyleSheet($url);
$category_id = JFactory::getApplication()->input->get('cid'); // id of product category
$auction_type = JFactory::getApplication()->input->get('atype'); // sale = 7 or rent = 6
$items_list_count = array(
	10,20,30,40,50
);
$items_сount = '10';
$limit = JFactory::getApplication()->input->get('limit');
if(!empty($limit)) {
	$items_сount = $limit;
}
?>
	<div class="header_additional_info">
<?php
if(in_array($category_id, array(AUCTIONS_CATEGORY, ORDERS_CATEGORY))) { // if category is 1 or 4
	if($category_id == ORDERS_CATEGORY) { // закупки
		echo "<a href=\"index.php/plany-zakupok\" style=\" color:#FFF; margin-right: 30px; text-decoration: underline;\">Планы закупок</a>";
	}
	?>
	<a href="index.php/normativnye-dokumenty" style=" color:#FFF; margin-right: 30px; text-decoration: underline;">Регламентирующие документы</a>
<?php
}
?>
		<span style="font-size: 12px;"><?php echo JText::_('COM_DJCATALOG2_TOTAL_RESULTS')." ".$this->getModel()->getTotal(); ?></span>
	</div>
<?php
switch($category_id) {
	case ORDERPLANS_CATEGORY: // plans of orders
		$fields = array(
			'name' => 'Название закупки',
			'file_1' => 'План закупки',
			'file_2' => 'Приложение'
		);
		?>
			<table class="zakypki">
				<thead>
				<?php foreach($fields as $id => $name) { ?>
					<th class="th-<?php echo $name; ?>"><?php echo $name; ?></th>
				<?php } ?>
				</thead>
				<tbody>
		<?php

			foreach ($this->items as $item) {
				$current_creator = $item->created_by;
				$files = DJCatalog2FileHelper::getFiles('item',$item->id);
		?>
					<tr>
						<td><?php echo $item->name; ?></td>
						<?php if(!empty($files)) {?>
								<td>
									<?php if(!empty($files[0])) { ?>
									<a target="_blank" class="button" href="<?php echo ('index.php?option=com_djcatalog2&format=raw&task=download&fid='.$files[0]->id);?>">
										<span><?php echo $files[0]->caption; ?></span>
									</a>
									<?php } ?>
								</td>
								<td>
									<?php
										$i = 1;
										while(!empty($files[$i])) {
									?>

										<a target="_blank" class="button" href="<?php echo ('index.php?option=com_djcatalog2&format=raw&task=download&fid='.$files[1]->id);?>">
											<span><?php echo $files[$i]->caption; ?></span>
										</a>
										<br />
									<?php
											$i++;
										}
									?>
								</td>

						<?php } else { ?>
								<td></td>
								<td></td>
						<?php } ?>
					</tr>
		<?php } ?>
				</tbody>
			</table>

		<?php
		break;
	case AUCTIONS_CATEGORY: // renting
	case REALTYOBJS_CATEGORY: // realty objects
	case ORDERS_CATEGORY: // orders
		$custom_statuses = DJCatalog2CustomHelper::getCustomStatuses();

		$result_status_tables = array();
		$attributes_list = array();
		$item_values = array();
		$realty_id = 0; // realty id of current item

		//usort($this->items, "order_by_date"); // custom sorting

		$item_category_to_attribute_group = array(
			1 => '2', // аренда
			3 => '3', // объекты
			4 => '1' // закупки
		);

		$headers = array("Название" => true);

		foreach ($this->items as $item) {
			$current_creator = $item->created_by;
			$status_zakupki = implode(', ', ($item->{'_ef_status-zakupki'}) ?  $item->{'_ef_status-zakupki'} : $item->{'_ef_status_zakupki_arenda'});
			$item_values[$item->id] = array();
			$row_content = "";

			if ($this->params->get('items_show_attributes', '1')) {
				?>
				<?php
				if (count($this->attributes) > 0) {
					foreach ($this->attributes as $attribute) {
						$attribute_value = '';
						$attribute_alias = '_ef_'.$attribute->alias; // for custom fields

						if(!empty($item->$attribute_alias)) {
							if (is_array($item->$attribute_alias)) {
								$item->$attribute_alias = implode(', ', $item->$attribute_alias);
							}

							if ($attribute->type == 'textarea' || $attribute->type == 'text'){
								$value = nl2br(htmlspecialchars($item->$attribute_alias));
								// convert URLs
								$value = preg_replace('#([\w]+://)([^\s()<>]+)#iS', '<a target="_blank" href="$1$2">$2</a>', $value);
								// convert emails
								$value = preg_replace('#([\w.-]+(\+[\w.-]+)*@[\w.-]+)#i', '<a target="_blank" href="mailto:$1">$1</a>', $value);
								$attribute_value = $value;
							}
							else if ($attribute->type == 'html') {
								$attribute_value = JHTML::_('content.prepare', $item->$attribute_alias);
							}
							else if ($attribute->type == 'calendar') {
								if(!empty($item->$attribute_alias))
									$attribute_value = JHtml::_('date', $item->$attribute_alias, JText::_('DATE_FORMAT_LC4'));
							}
							else {
								$attribute_value = htmlspecialchars($item->$attribute_alias);
							}

							$item_values[$item->id][$attribute_alias] = $attribute_value;
						}
					}
					?>
				<?php } ?>
			<?php
			}

			$row_content .= "
				<tr data-itemid = '{$item->id}'>
			";
			// building first row with link to item
			$link = (JHTML::link(JRoute::_(DJCatalogHelperRoute::getItemRoute($item->slug, $item->catslug )), $item->name));
			$row_content .= "<td>".$link."</td>";

			foreach($this->attributes as $attribute) {
				$attribute_alias = '_ef_'.$attribute->alias;
				$attribute_name = $attribute->name;
				if($attribute->group_id == $item_category_to_attribute_group[$category_id]) {
					$headers[$attribute_name] = $attribute_alias;
					if(isset($item_values[$item->id][$attribute_alias])) {
						if($attribute_alias == '_ef_koordinaty' && $category_id == REALTYOBJS_CATEGORY) {
							$attr_value = $item_values[$item->id][$attribute_alias];
							$attr_value = explode(',', $attr_value);
							$row_content .= "<td class='td".$attribute_alias."'>".implode("<br>", $attr_value)."</td>";
						} else {
							$row_content .= "<td class='td".$attribute_alias."'>".$item_values[$item->id][$attribute_alias]."</td>";
						}
					} else {
						$row_content .= "<td class='td".$attribute_alias."'></td>";
					}
				}
				if($category_id == AUCTIONS_CATEGORY) { // get additional attributes for rent object from appropriate realty object
					if($attribute->group_id == $item_category_to_attribute_group['3']) { // only attributes for realty objects
						$headers[$attribute_name] = $attribute_alias;
						$realty_id = $item->{'_ef_id_pomeshcheniya'}; // getting id of realty object
						$realty_item = get_related_object_item($realty_id);
						if(!empty($realty_item->$attribute_alias)) {
							if (is_array($realty_item->$attribute_alias)) {
								$realty_item->$attribute_alias = implode(', ', $realty_item->$attribute_alias);
							}

							if ($attribute->type == 'textarea' || $attribute->type == 'text'){
								$attr_value = nl2br(htmlspecialchars($realty_item->$attribute_alias));
								// convert URLs
								$attr_value = preg_replace('#([\w]+://)([^\s()<>]+)#iS', '<a target="_blank" href="$1$2">$2</a>', $attr_value);
								// convert emails
								$attr_value = preg_replace('#([\w.-]+(\+[\w.-]+)*@[\w.-]+)#i', '<a target="_blank" href="mailto:$1">$1</a>', $attr_value);
								$attribute_value = $attr_value;
							}
							else if ($attribute->type == 'html') {
								$attribute_value = JHTML::_('content.prepare', $realty_item->$attribute_alias);
							}
							else if ($attribute->type == 'calendar') {
								if(!empty($item->$attribute_alias))
									$attribute_value = JHtml::_('date', $realty_item->$attribute_alias, JText::_('DATE_FORMAT_LC4'));
							}
							else {
								$attribute_value = htmlspecialchars($realty_item->$attribute_alias);
							}

							$row_content .= "<td class='td".$attribute_alias."'>".$attribute_value."</td>";
						} else {
							$row_content .= "<td class='td".$attribute_alias."'></td>";
						}
					}
				}
			}

			$row_content .="
				</tr>
			";
			if($category_id != REALTYOBJS_CATEGORY) { // realty objects without dividing on statuses
				$status_key = array_search($status_zakupki, $custom_statuses);
				$result_status_tables[$status_key][] = $row_content;
			} else {
				$result_status_tables[0][] = $row_content;
			}
		}



		?>
			<ul id="tab" class="nav nav-tabs" data-tabs="tabs" style="border-bottom: none;">
		<?php
		if($category_id != REALTYOBJS_CATEGORY) { // realty objects without dividing on statuses
			?>

				<?php
				foreach($custom_statuses as $index => $name) {
					if(!(int)$index) {
						echo "<li class='active'><a href='#status_{$index}' data-toggle='tab'>".$name."</a></li>";
					} else {
						echo "<li><a href='#status_{$index}' data-toggle='tab'>".$name."</a></li>";
					}

				}
				?>
				<li>
					<a href='#last_tab' data-toggle='tab'>Поиск</a>
				</li>
				<li>
					<select class="djc_select" name="count" id="count" onchange="change_listing_count(this)">
						<?php
						foreach($items_list_count as $list_count) {
							if($items_сount == $list_count) {
								echo '<option selected="selected" value="'.$list_count.'">Выводить '.$list_count.' позиций</option>';
							} else {
								echo '<option value="'.$list_count.'">Выводить '.$list_count.' позиций</option>';
							}
						}
						if($items_сount == 'all') {
							echo '<option value="all" selected="selected">Показать все</option>';
						} else {
							echo '<option value="all">Показать все</option>';
						}

						?>
					</select>
				</li>
			</ul>
			<div id="my-tab-content" class="tab-content">
				<?php
				$active_set = false;
				foreach($custom_statuses as $key => $name) {
					if(!$active_set) {
						echo "<div id='status_{$key}' class='table_tab tab-pane active'>";
						$active_set = true;
					} else {
						echo "<div id='status_{$key}' class='table_tab tab-pane'>";
					}

					echo 	"<table class='zakypki'>
							<col width='10%'>
							<col width='10%'>
							<col width='10%'>
							<col width='10%'>
							<col width='10%'>
							<col width='10%'>
							<col width='10%'>
							<col width='10%'>
							<thead>";

					foreach($headers as $name => $value) {
						echo "<th class='th".$value."'>$name</th>";
					}

					?>
							</thead>
							<tbody>
							<?php
							foreach($result_status_tables[$key] as $row ){
								echo $row;
							}
							?>
							</tbody>
						</table>
					</div>
				<?php
				}
				?>
				<div id="last_tab" class="table_tab tab-pane">
					<form action="<?php echo JRoute::_('index.php?option=com_djcatalog2&task=search'); ?>" method="post" name="DJC2searchForm" id="DJC2searchForm" >
						<fieldset class="djc_mod_search djc_clearfix">
							<input type="text" class="inputbox" name="search" id="mod_djcatsearch" value="" style="float: left;"/>
							<input type="submit" style="float: left; margin: 0 0 0 5px;" class="btn-primary btn submit_btn" onclick="document.DJC2searchForm.submit();" value="Поиск" />
						</fieldset>

						<input type="hidden" name="option" value="com_djcatalog2" />
						<input type="hidden" name="view" value="items" />
						<input type="hidden" name="cid" value="<?php echo $category_id; ?>" />
						<?php
						if(!empty($auction_type)) {
							?>
							<input type="hidden" name="atype" value="<?php echo $auction_type; ?>" />
							<?php
						}
						?>
						<input type="hidden" name="task" value="search" />
						<input type="submit" style="display: none;"/>
					</form>
				</div>
			</div>
		<?php
		} else {
			// list of realty objects
			?>
				<li style="float: right;">
					<select class="djc_select" name="count" id="count" onchange="change_listing_count(this)">
						<?php
						foreach($items_list_count as $list_count) {
							if($items_сount == $list_count) {
								echo '<option selected="selected" value="'.$list_count.'">Выводить '.$list_count.' позиций</option>';
							} else {
								echo '<option value="'.$list_count.'">Выводить '.$list_count.' позиций</option>';
							}
						}
						if($items_сount == 'all') {
							echo '<option value="all" selected="selected">Показать все</option>';
						} else {
							echo '<option value="all">Показать все</option>';
						}
						?>
					</select>
				</li>
			</ul>
			<div id="my-tab-content" class="tab-content">
				<?php
				echo 	"
				<table class='zakypki'>
					<col width='30%'>
					<col width='10%'>
					<col width='10%'>
					<col width='10%'>
					<col width='10%'>
					<col width='10%'>
					<col width='10%'>
					<col width='10%'>
					<thead>
				";

				foreach($headers as $name => $value) {
					echo "<th class='th".$value."'>$name</th>";
				}?>

					</thead>
					<tbody>
					<?php
						foreach($result_status_tables[0] as $row ){
							echo $row;
						}
						?>
					</tbody>
				</table>
			</div>
		<?php
		}
		break;
}
$edit_auth = ($user->authorise('core.edit', 'com_djcatalog2') || ($user->authorise('core.edit.own', 'com_djcatalog2') && $user->id == $current_creator)) ? true : false;
if ((int)$this->params->get('fed_edit_button', 0) == 1 && $edit_auth) { ?>
	<div class="moderation_container">
		<a class="moderation_button" href="<?php echo JRoute::_('index.php?option=com_djcatalog2&task=itemform.edit&id='.$this->item->id); ?>">Модерация</a>
	</div>
<?php }

function order_by_date($a, $b) {
	if(!empty($a->{'_ef_data_auktsiona'})) {
		$a = strtotime($a->{'_ef_data_auktsiona'});
		$b = strtotime($b->{'_ef_data_auktsiona'});
	} elseif(!empty($a->{'_ef_data_auktsiona_arenda'})) {
		$a = strtotime($a->{'_ef_data_auktsiona_arenda'});
		$b = strtotime($b->{'_ef_data_auktsiona_arenda'});
	}

	if ($a == $b) {
		return 0;
	}
	return ($a > $b) ? -1 : 1;
}

function get_related_object_item($fid) {
	require_once(JPATH_COMPONENT.DS.'models'.DS.'item.php');

	$related_item_model = new DJCatalog2ModelItem();
	return $related_item_model->getItemByFK($fid);
}