<?php

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

?>

<?php if ($type == 'logout') : ?>

	<form class="uk-form" action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post">

		<?php if ($params->get('greeting')) : ?>
		<div class="uk-form-row"> <p style="color: #ffffff; margin: 0px">
			<?php if ($params->get('name') == 0) : {
				echo JText::sprintf('MOD_LOGIN_HINAME', htmlspecialchars($user->get('name')));
			} else : {
				echo JText::sprintf('MOD_LOGIN_HINAME', htmlspecialchars($user->get('username')));
			} endif; ?>
				<?php endif; ?>

					&nbsp<button class="uk-button uk-button-primary" value="<?php echo JText::_('JLOGOUT'); ?>" style="min-height: 20px; height: 20px; font-size: 11px" name="Submit" type="submit"><?php echo JText::_('JLOGOUT'); ?></button>
		</p></div>

		<input type="hidden" name="option" value="com_users">
		<input type="hidden" name="task" value="user.logout">
		<input type="hidden" name="return" value="<?php echo $return; ?>">
		<?php echo JHtml::_('form.token'); ?>
	</form>

<?php endif; ?>
