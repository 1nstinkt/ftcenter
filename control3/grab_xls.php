<?php


error_reporting(E_ERROR | E_WARNING | E_PARSE);
set_time_limit(0);

date_default_timezone_set('Europe/Moscow');

?>
<!DOCTYPE html>
<html>

<head>
    <title>ФТ-Центр. Карта объектов: панель управления.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

 	    <link href="bootstrap.min.css" rel="stylesheet">

	<style> 
	.bg-danger {
		
	}
	.ft_info {
		font-size:20px;
		padding: 15px;
	}
	</style>
</head>

<body>
 	<div class="container">

	
<?php


if(count($_POST)<1 || (!isset($_POST['action']))   ||  !($_POST['action']=='renew' ||  $_POST['action']=='get_coords'))
	{
		echo '<p class="bg-danger ft_info">Действие не выбрано. <a href="index.php">Перейти в панель управления</a></p>';
		die();
	}
	
if(count($_FILES)!=1)
	{
		echo '<p class="bg-danger ft_info">Файл не загружен. <a href="index.php">Перейти в панель управления</a></p>';
		die();
	}	
	

 
 
/** PHPExcel_IOFactory */
include './Classes/PHPExcel/IOFactory.php';


 

move_uploaded_file($_FILES['ft_file']['tmp_name'], './'.$_FILES['ft_file']['name']);


$inputFileName = './'.$_FILES['ft_file']['name'];


if($_POST['action']=='get_coords')
{

	$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);	 	
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

	echo "<div id='before_text'>Всего объектов: <b>".count($sheetData)."</b><br/><i>Пожалуйста, дождитесь окончания работы скрипта</i><hr/></div>";
	

	$result='		
	<script type="text/javascript">

		document.getElementById("before_text").style.display = "none";
		
		function selectElementContents(el) {
			var body = document.body, range, sel;
			if (document.createRange && window.getSelection) {
				range = document.createRange();
				sel = window.getSelection();
				sel.removeAllRanges();
				try {
					range.selectNodeContents(el);
					sel.addRange(range);
				} catch (e) {
					range.selectNode(el);
					sel.addRange(range);
				}
			} else if (body.createTextRange) {
				range = body.createTextRange();
				range.moveToElementText(el);
				range.select();
			}
		}
	</script>
		
		
			<h3>Скопируйте результаты в файл .xls для дальнейшего использования</h3>
			<button type="button" class="btn btn-primary" onclick="selectElementContents( document.getElementById(\'result_table\') );">Скопировать таблицу (после нажатия на кнопку нажмите ctrl+C и вставьте таблицу в файл xls)</button>
			 
			<a href="index.php" class=" btn btn-default" style="float: right;">Перейти в панель управления (незабудьте скопировать таблицу)</a>

			<table class="table" id="result_table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Адрес</th>
						<th>Координаты</th>
						<th>Информация</th>
						<th>Ссылка</th>
					</tr>
				</thead>
				<tbody>
	';


	
	$i=0;
	$FailedAddress=  array();
	foreach($sheetData as $row) {
		
		if($i==0)
		{
			$i++;
			continue;
		}
		
			 
			if(!isset($row["A"]) || !isset($row["B"]) || $row["A"]==""  || $row["B"]=="")
				{
				
					if(!isset($row["A"]) && !isset($row["B"]) )
						break;				
				
					echo '<p class="bg-danger ft_info">Не верно отформатированный файл xls, строка '.($i+1).'</p>';
					break;
				}
		 
			
			$id=$row["A"];
			$address=htmlspecialchars($row["B"]);
				
			$info="";
			if(isset($row["D"])) 
				$info=$row["D"];
				
			$link="";	
			if(isset($row["E"])) 
				$link=$row["E"];
			
			
			if(isset($row["C"]) && $row["C"]!="") 
			{
				$coords=$row["C"];

				$result.="
					<tr>
						<td>$id</td>
						<td>$address</td>
						<td>$coords</td>
						<td>$info</td>
						<td>$link</td>
					</tr>
				";						
				
			}
			else
			{				
				$coords="";
				
				$xml = simplexml_load_file('http://geocode-maps.yandex.ru/1.x/?geocode='.urlencode($row["B"]));

				$found = $xml->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found;
			 
	
			
			
				if ($found > 0) {

					$pos=$xml->GeoObjectCollection->featureMember->GeoObject->Point->pos;								
					
					$pos=explode(" ",$pos);
					
					$coords = $pos[1].",".$pos[0];
					//$coords = str_replace(' ', ',', $xml->GeoObjectCollection->featureMember->GeoObject->Point->pos);
					/*echo "<PRE>";					print_r($xml->GeoObjectCollection->featureMember->GeoObject);					echo "</PRE>";*/					 					
					$countGeocodeSuccess++;
					
					$result.="
						<tr>
							<td>$id</td>
							<td>$address</td>
							<td>$coords</td>
							<td>$info</td>
							<td>$link</td>
						</tr>
					";		
					

				} else {			
				  $countGeocodeFault++;
				  array_push($FailedAddress,$address);
				}			
			}	
				
			
				
		
		$i++;	
	}

	$result.='
		</tbody>
	</table>
	';
	
	if($countGeocodeFault>0) 
		{
			$n=$countGeocodeFault+1;
			//echo "<script type='text/javascript'>alert('Яндекс не смог найти $n объектов. Смотри список внизу страницы.')</script>";
			echo "<h3>Яндекс не смог найти следующие объекты (они не включены в итоговую таблицу)</h3>";
			$j=1;
			foreach($FailedAddress as $addr)
			{
				echo $j.")<span style='color:red;'>$addr</span><br/>";
				$j++;
			}
		}
		
		echo $result;
	
}


 

if($_POST['action']=='renew') 
	{

	$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);	 	



	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	 
	//echo "<PRE>";
	//var_dump($sheetData);
	//echo "</PRE>";
	 

	$countGeocodeSuccess=0;
	$countGeocodeFault=0;

	$GeoArray=Array();

	class GeoObject
	{
		// property declaration
		public $id="";
		public $address="";
		public $info="";
		public $coords="";
		public $coords_ln="";
		public $coords_lt="";
		public $link="";

	   function __construct($id,$address,$cooords,$info="",$link="") {
			$this->id=$id;
			$this->address=$address;
			$this->coords=$cooords;
			$this->info=$info;
			$this->link=$link;
			
/*			
			$mod1=rand(-9999,9999)/1000;
			$mod2=rand(-9999,9999)/1000;
			
			$this->coords_ln=55.763338+$mod1;
			$this->coords_lt=37.565466+$mod1;
	 */
		
		 //  print "In BaseClass constructor<br/>";
	   }
		
	   function echo_GeoItem()
		{
			//return '{"id":"'.$this->id.'","address":"'.$this->address.'","info":"'.$this->info.'","point":['.$this->coords_ln.','.$this->coords_lt.']}';
			return '{"id":"'.$this->id.'","address":"'.$this->address.'","info":"'.$this->info.'","link":"'.$this->link.'","point":['.$this->coords.']}';
		}
		
	}



	//echo "Всего объектов: <b>".count($sheetData)."</b><br/><i>Пожалуйста, дождитесь окончания работы скрипта</i><br/><br/>";
	 
	 $i=0;
	foreach($sheetData as $row) {

	if($i==0)
		{
			$i++;
			continue;
		}

		
		if(!isset($row["A"]) || !isset($row["B"])   || !isset($row["C"]) )
			{
			
				
				if(!isset($row["A"]) && !isset($row["B"])   && !isset($row["C"]) )
					break;
					
				echo '<p class="bg-danger ft_info">Не верно отформатированный файл xls - пусто</p>';
				echo $row["A"]."<hr/>".$row["B"]."<hr/>".$row["C"]."<hr/>";
				break;
			}
			
			
			if($row["A"]=="" || $row["B"]=="" || $row["C"]=="")
			{
				
				echo '<p class="bg-danger ft_info">Не верно отформатированный файл xls</p>';
				echo $row["A"]."<hr/>".$row["B"]."<hr/>".$row["C"]."<hr/>";
				break;
			}			
			
			
			
	 
		
		$id=$row["A"];
		$address=htmlspecialchars($row["B"]);
		$coords=$row["C"];
			
		$info="";
		if(isset($row["D"])) 
			$info=$row["D"];
			
		$link="";
		if(isset($row["E"])) 
			$link=$row["E"];			
		
		
		
		array_push($GeoArray,new GeoObject($id,$address,$coords,$info,$link)); 
		$i++;
		
	}



	$result="var GeoJSON='[";
	foreach ($GeoArray as $GeoItem)  {
		$result.=$GeoItem->echo_GeoItem().",";
	}
	$result=rtrim($result, ",");

	$result.="]';";


	file_put_contents('../js/ftmap_objects.js',$result);
	//echo "<PRE>";
	//echo $result;
	//echo "</PRE>";
	
	?>
		<h3>Файл с информацией об объектах заменён</h3>
		<p class="ft_info bg-success">
		Пользователи увидят результат после обновления всех уровней кэша.  <a href="index.php">Перейти в панель управления</a>
		<br/>
		</p>
	<?
	

	
}	


unlink($inputFileName);




?>
	</div>	 
 	 
</body>

 
</html>