<!DOCTYPE html>
<html>

<head>
    <title>ФТ-Центр. Карта объектов: панель управления.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

 	    <link href="bootstrap.min.css" rel="stylesheet">

	<style> 
	</style>
</head>

<body>
 	<div class="container">
		 <h2>ФТ-Центр. Карта объектов: панель управления.</h2>
		 

		<h3>Обновление информации на сайте</h3>
		<h4>Формат файла (первая строка - всегда заголовок, пустые строки не допускаются):</h4>
		<table class="table">		
			<tr><th>ID - Уникальный числовой индефикатор</th><th>Адрес - Полный адрес</th><th>Координаты</th><th>Описание - любой текст</th><th>Ссылка на карточку объекта</th></tr>			
			<tr><td>Например: <i>10</i></td><td>Например: <i>г. Москва ул. Гиляровского дом 31</i></td><td>Например: <i>57.442338,39.244466</i></td><td>Например: <i>Общая площадь: 1200 кв. м.</i></td><td>Например: <i>http://ftcenter.ru/object1</i></td></tr>
		</table>
		<blockquote>            
              <p>Все ячейки не должны быть пустыми кроме ячеек  <b>Описания</b> и <b>Ссылки</b>.</p>
        </blockquote>
		<h4>Обработка файла</h4>	
		<form enctype="multipart/form-data"  action="grab_xls.php" method="POST">
			  <div class="form-group">
				<label for="exampleInputFile" >Файл</label>
				<input type="file" id="exampleInputFile" name="ft_file">
				<p class="help-block">*.xls</p>
			  </div>
				<input type="hidden" name="MAX_FILE_SIZE" value="300000" />
			  <input type="hidden" value="renew"  name="action"/>
			  <button type="submit" class="btn btn-primary">Обработать и обновить информацию на сайте</button>			  
		 </form>
		 <hr/>		 
		 
		 
		<h3>Получение координат</h3>
		<h4>Формат файла (первая строка - всегда заголовок, пустые строки не допускаются):</h4>
		<table class="table">		
			<tr><th>ID - Уникальный числовой индефикатор</th><th>Адрес - Полный адрес</th><th>Координаты</th><th>Описание - любой текст</th><th>Ссылка на карточку объекта</th></tr>			
			<tr><td>Например: <i>10</i></td><td>Например: <i>г. Москва ул. Гиляровского дом 31</i></td><td>Например: <i>57.442338,39.244466</i></td><td>Например: <i>Общая площадь: 1200 кв. м.</i></td><td>Например: <i>http://ftcenter.ru/object1</i></td></tr>
		</table>
		<blockquote>            
              <p>Все ячейки не должны быть пустыми кроме ячеек <b>Координат</b>, <b>Описания</b> и <b>Ссылки</b>.</p>
			  <p>Если ячейка с Координат будет пуста, скрипт запросит координаты у сервиса Яндекс.Карты.</p>
        </blockquote>		
		<h4>Обработка файла</h4>	
 		<form enctype="multipart/form-data"  action="grab_xls.php" method="POST">
			  <div class="form-group">
				<label for="exampleInputFile">Файл</label>
				<input type="file" id="exampleInputFile" name="ft_file">
				<p class="help-block">*.xls</p>
			  </div>
			  <input type="hidden" name="MAX_FILE_SIZE" value="300000" />
			  <input type="hidden" value="get_coords"  name="action"/>
			  <button type="submit" class="btn btn-primary">Обработать и получить результат</button>
		 </form>
		 <hr/>
	</div>	 
 	 
</body>

 
</html>

 


