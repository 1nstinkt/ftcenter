//http://api.yandex.ru/maps/jsbox/custom_search
//http://api.yandex.ru/maps/jsbox/apply_bounds_and_clusterize

ymaps.ready(function () {
	var GeoArray = [];

	var myMap = window.map = new ymaps.Map('YMapsID', {
		center: [55.751574, 37.573856],
		zoom: 9,
		behaviors: ['default']
	});


	myMap.controls
		// Кнопка изменения масштаба.
		.add('zoomControl', { left: 5, top: 5 })
		// Список типов карты
		.add('typeSelector')
		// Стандартный набор кнопок
		.add('mapTools', { left: 35, top: 5 });


	var clusterer = new ymaps.Clusterer({
		preset: 'twirl#invertedRedClusterIcons',
		groupByCoordinates: false,
		clusterDisableClickZoom: true
	});

	var getPointData = function (index) {

		var address = "---";
		var info = "";
		var image = "";


		if (typeof GeoArray[index].address != 'undefined')
			address = GeoArray[index].address;

		if (typeof GeoArray[index].link != 'undefined' && GeoArray[index].link != "")
			address = '<a href="' + GeoArray[index].link + '" title="' + address + '" target="_blank">' + address + '</a>';


		if (typeof GeoArray[index].info != 'undefined')
			info = GeoArray[index].info;


		return {
			balloonContentHeader: address,
			balloonContentBody: image,
			balloonContentFooter: info,
			clusterCaption: "<b>" + address + "</b>"
		};
	};

	var getPointOptions = function () {
		return {
			preset: 'twirl#redIcon'
		};
	};

	jQuery(document).ready(function ($) {
		jQuery.getJSON('index.php?option=com_djcatalog2&view=item&task=getAllRealtyObjects&format=json', {}, function (data) {
			GeoArray = data;

			var geoObjects = [];


			for (var i = 0, len = GeoArray.length; i < len; i++) {

				var geoObject = new ymaps.Placemark(GeoArray[i].point, getPointData(i), getPointOptions());

				geoObject.ft_id = GeoArray[i].id;

				geoObject.events.add('balloonopen', function (e) {
					var cur_obj = this;


					if (cur_obj.properties.get('balloonContentBody') != "") {
						//Для группы картинок включим режим галлереи
						$(".ft_cbox_" + cur_obj.ft_id).colorbox({rel: cur_obj.ft_id});
						return false;
					}

					cur_obj.properties.set('balloonContentBody', "Картинка загружается");

					$.get("return_photo.php?id=" + cur_obj.ft_id, function (data) {
						cur_obj.properties.set('balloonContentBody', "<center>" + data + "</center>");
						//Для группы картинок включим режим галлереи
						$(".ft_cbox_" + cur_obj.ft_id).colorbox({rel: cur_obj.ft_id});
					});

				}, geoObject);

				geoObjects[i] = geoObject;


			}


			clusterer.options.set({
				gridSize: 80,
				clusterDisableClickZoom: false
			});


			clusterer.add(geoObjects);


			clusterer.events.once('objectsaddtomap', function () {
				myMap.setBounds(clusterer.getBounds(), {
						checkZoomRange: true
					}
				);
			});


			clusterer.events.add('objectsaddtomap', function () {
				for (var i = 0, len = geoObjects.length; i < len; i++) {
					var geoObject = geoObjects[i],

						geoObjectState = clusterer.getObjectState(geoObject),
					// признак, указывающий, находится ли объект в видимой области карты
						isShown = geoObjectState.isShown,
					// признак, указывающий, попал ли объект в состав кластера
						isClustered = geoObjectState.isClustered,
					// ссылка на кластер, в который добавлен объект
						cluster = geoObjectState.cluster;

					/*
					 if(window.console) {
					 console.log('Геообъект: %s, находится в видимой области карты: %s, в составе кластера: %s', i, isShown, isClustered);
					 }*/

				}
			});

			myMap.geoObjects.add(clusterer);
		}, 'json')

	});


	//console.log(obj);



});




