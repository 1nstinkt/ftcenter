<?

define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);

if (file_exists(dirname(__FILE__) . '/defines.php')) {
	include_once dirname(__FILE__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', dirname(__FILE__));
	require_once JPATH_BASE.'/includes/defines.php';
}

require_once JPATH_BASE.'/includes/framework.php';

function main() {
	$path="./media/djcatalog2/images/";

	if(!isset($_GET['id'])) 
		return false;

	$id = intval($_GET['id']);
	$images = getImages($id);
	if(!$images) return false;

	foreach($images as $image){
		echo "<a class='ft_object_image_link ft_cbox_$id' href='".$path.$image->fullpath."'   target='_blank' ><img class='ft_object_image' src='".$path.$image->path."/".$image->name."_m.".$image->ext."' rel='$id' /></a>";
	}
	
	return true;
}

if(!main())
{ 
	if($showBlankImage)
		echo "<img class='ft_object_image' src='./img/no_image.png'/>";
}

function getImages($id) {
	require_once ( JPATH_BASE . DS . 'includes' . DS . 'framework.php' );

	$db = JFactory::getDbo();

	$db->setQuery('SELECT * '.
	' FROM #__djc2_images '.
	' WHERE item_id='.$id.
	' 	AND type="item" '.
	' ORDER BY ordering ASC, name ASC ');
	$images = $db->loadObjectList();

	if (count($images)) {
		return $images;
	}
	return false;
}
?>